import type { Question } from "@/models/question";
import type { Category } from "@/models/category";
import { defineStore } from "pinia";

export type RootState = {
  questions: Question[];
  categories: Category[];
  tags: string[];
};

function migrateTagsFromCategories(
  categories: Category[],
  questions: Question[]
) {
  questions.filter((question) => {
    const categoryname = categories.find(
      (category) => category.uuid === question.categoryUuid
    )?.name;
    if (question.tags === undefined) {
      question.tags = [];
    }
    if (
      categoryname !== undefined &&
      (question.tags === undefined || question.tags.length == 0)
    ) {
      question.tags.push(categoryname);
    }
  });
}

export const useMainStore = defineStore({
  id: "mainStore",
  state: () => {
    const stringQuestions = localStorage.getItem("questions");
    let parsedQuestions: Question[];
    if (stringQuestions) {
      parsedQuestions = JSON.parse(stringQuestions);
    } else {
      parsedQuestions = [];
    }

    const stringCategories = localStorage.getItem("categories");
    let parsedCategories: Category[];
    if (stringCategories) {
      parsedCategories = JSON.parse(stringCategories);
    } else {
      parsedCategories = [];
    }

    /* migration functions for local storage */
    migrateTagsFromCategories(parsedCategories, parsedQuestions);

    /* calc known tags*/
    let tags: string[] = [];
    for(let question of parsedQuestions) {
      tags = tags.concat(question.tags);
      /* removes duplicates */
      tags = tags.filter((item, index) => {
        return (tags.indexOf(item) == index)
      })
    }

    return {
      questions: parsedQuestions,
      categories: parsedCategories,
      tags: tags
    } as RootState;
  },

  actions: {
    saveToBrowserStore() {
      localStorage.setItem("questions", JSON.stringify(this.$state.questions));
      localStorage.setItem(
        "categories",
        JSON.stringify(this.$state.categories)
      );
    },

    createNewquestion(question: Question) {
      if (!question) return;

      this.questions.push(question);
      this.saveToBrowserStore();
    },

    createNewcategory(category: Category) {
      if (!category) return;

      this.categories.push(category);
      this.saveToBrowserStore();
    },

    deletequestion(index: number) {
      this.questions.splice(index, 1);
      this.saveToBrowserStore();
    },

    deletecategory(index: number) {
      this.categories.splice(index, 1);
      this.saveToBrowserStore();
    },
  },
});
