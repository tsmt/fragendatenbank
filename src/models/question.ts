export type Question = {
  uuid: string;
  text: string;
  categoryUuid: string;
  tags: Array<string>;
  notes: string;
  infotext: string;
  fullTextAnswer: boolean;
  range: {
    enable: boolean;
    leftScale: string;
    rightScale: string;
    leftScaleEmoji: string;
    rightScaleEmoji: string;
  };
  multipleChoice: {
    enable: boolean;
    choices: Array<string>;
  };
  profileBasedAnswers: boolean;
  allowLists: boolean;
  negativeList: Array<string>;
  neutralList: Array<string>;
  positiveList: Array<string>;
};
